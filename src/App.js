import React, { Component } from 'react';
import './App.css';
import Navigation from './components/Navigation';
import Content from './components/Content';
import 'antd/dist/antd.css';

class App extends Component {

  render() {
    return (
      <div className="App">
        <Navigation/>
        <br/>
        <div className="content">
          <Content />
        </div>
      </div>
    );
  }
}

export default App;
