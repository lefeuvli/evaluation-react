import React, {Component} from 'react';
import DEFAULT_STATE from '../initState';
import axios from 'axios';
import { Table, Button, Card } from 'antd';

const PATH_BASE = "https://jsonplaceholder.typicode.com"

class Utilisateurs extends Component {
    constructor(props) {
      super(props);
      this.state = DEFAULT_STATE
    }

    componentDidMount() {
        this.getUsers()
        this.setState({displayPosts: false, displayAlbums: false, displayTodos: false})
    }

    setResult = (result) => {
        this.setState({ result: result });
    }

    setDetailResultPosts = (result) => {
        this.setState({ detailData: result, displayPosts: true, displayAlbums: false, displayTodos: false });
    }

    setDetailResultAlbums = (result) => {
        this.setState({ detailData: result, displayAlbums: true, displayPosts: false, displayTodos: false });
    }

    setDetailResultTodos = (result) => {
        this.setState({ detailData: result, displayAlbums: false, displayPosts: false, displayTodos: true });
    }

    getUsers = () => {
        axios(`${PATH_BASE}/users`)
             .then(result => this.setResult(result.data))
             .catch(error => this.setState({ error }));
    }

    onTapPosts = (data) => {
        axios(`${PATH_BASE}/posts?userId=${data.id}`)
             .then(result => this.setDetailResultPosts(result.data))
             .catch(error => this.setState({ error }));
    }

    onTapAlbums = (data) => {
        axios(`${PATH_BASE}/albums?userId=${data.id}`)
             .then(result => this.setDetailResultAlbums(result.data))
             .catch(error => this.setState({ error }));
    }

    onTapTodos = (data) => {
        axios(`${PATH_BASE}/todos?userId=${data.id}`)
             .then(result => this.setDetailResultTodos(result.data))
             .catch(error => this.setState({ error }));
    }

    deleteUser = (id) => {
        const updatedList = this.state.result.filter((item) => item.id !== id)
        this.setState({result: updatedList})
    }

    render() {
        const {result, detailData, displayPosts, displayAlbums, displayTodos} = this.state
        if (!result) { return null; }

        const columns = [{
            title: 'name',
            dataIndex: 'name',
            key: 'name'
          },
          {
            title: 'username',
            dataIndex: 'username',
            key: 'username'
          },
          {
            title: 'email',
            dataIndex: 'email',
            key: 'email'
          },
          {
            title: 'address',
            dataIndex: 'address',
            key: 'address',
            render: data => (
                <p>{data.street} ({data.suite}) - {data.zipcode} {data.city}</p>
            )
          },
          {
            title: 'Action',
            key: 'action',
            render: data => (
              <span>
                <Button type="primary" onClick={() => this.onTapPosts(data)}>Posts</Button>
                <br/>
                <br/>
                <Button type="primary" onClick={() => this.onTapAlbums(data)}>Albums</Button>
                <br/>
                <br/>
                <Button type="primary" onClick={() => this.onTapTodos(data)}>Todos</Button>
                <br/>
                <br/>
                <Button type="danger" onClick={() => this.deleteUser(data.id)}>Delete</Button>
              </span>
            ),
          }
        ]

        const postsColums = [
            {
                title: "Title",
                dataIndex: "title",
                key: "title"
            },
            {
              title: 'Body',
              dataIndex: 'body',
              key: 'body'
            }
        ]

        const albumsColumns = [
            {
                title: "Title",
                dataIndex: "title",
                key: "title"
            }
        ]

        const todosColumns = [
            {
                title: "Title",
                dataIndex: "title",
                key: "title"
            },
            {
                title: "Completed",
                dataIndex: "completed",
                key: "completed",
                render: data => (
                    <p>{data? "Yes": "No"}</p>
                )
            }
        ]

        return (
            <div>
                <Table columns={columns} dataSource={result} />
                {displayPosts &&
                    <Card
                    title="Posts" >
                        <Table columns={postsColums} dataSource={detailData}/>
                    </Card>
                }
                {(displayAlbums) &&
                    <Card
                    title="Albums" >
                        <Table columns={albumsColumns} dataSource={detailData}/>
                    </Card>
                }
                {(displayTodos) &&
                    <Card
                    title="Todos" >
                        <Table columns={todosColumns} dataSource={detailData}/>
                    </Card>
                }
            </div>
        )
    }
}
export default Utilisateurs