import React, {Component} from 'react';
import Formulaire from '../components/Formulaire';
import DEFAULT_STATE from '../initState';
import axios from 'axios';
import { Button } from 'antd';

const PATH_BASE = "https://jsonplaceholder.typicode.com"

class Accueil extends Component {
    constructor(props) {
      super(props);
      this.state = DEFAULT_STATE
    }

    getUser = (mail) => {
      axios(`${PATH_BASE}/users/?email=${mail}`)
           .then(result => this.setResult(result.data))
           .catch(error => this.setState({ error }));
    }


    setResult = (user) => {
      console.log(user)
      if(user.length === 1 ){
        this.setState({connectedUser: user[0]});
      } else {
        this.setState({connectedUser: null});
      }

    }

    handleSubmit = event => {
      event.preventDefault();
      this.getUser(this.state.login);

    };

    handleChange = event => {
        this.setState({
          [event.target.id]: event.target.value
        });
    };

    logout = () => {
      this.setState({connectedUser: null})
    }

    render() {

    const tabChamps = [
        {label: "Login : ", id: "login", type: "text",value:this.state.login }
    ];

        return (
          <div>
            {this.state.connectedUser ? (
              <div>
                <p>{this.state.connectedUser.name}</p>
                <Button onClick={this.logout}>Deconnexion</Button>
              </div>
            ) : (
              <Formulaire
              soumission = {this.handleSubmit}
              changement = {this.handleChange}
              tabChamps = {tabChamps}
              submitText = "Connexion"
               ></Formulaire>
            )}
          
            <h1>Bienvenue sur le site ! </h1>
          </div>
        )
    }
}
export default Accueil