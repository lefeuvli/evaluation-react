import React, {Component} from 'react';
import DEFAULT_STATE from '../initState';
import axios from 'axios';
import { Table, Button, Card } from 'antd';
import Formulaire from '../components/Formulaire';

const PATH_BASE = "https://jsonplaceholder.typicode.com"

class Articles extends Component {
    constructor(props) {
      super(props);
      this.state = DEFAULT_STATE
    }

    componentDidMount() {
        this.getArticles()
        this.setState({displayDetail: false, searchTerm: ""})
    }

    setResult = (result) => {
        this.setState({ result: result });
    }

    getArticles = () => {
        axios(`${PATH_BASE}/posts`)
             .then(result => this.setResult(result.data))
             .catch(error => this.setState({ error }));
    }
    
    getUserArticles = () => {
        axios(`${PATH_BASE}/posts?userId=${this.state.userId}`)
             .then(result => this.setResult(result.data))
             .catch(error => this.setState({ error }));
    }

    getComments = (postId) => {
        axios(`${PATH_BASE}/comments?postId=${postId}`)
        .then(result => this.setArticleComments(result.data))
        .catch(error => this.setState({error}));
    }

    setArticleComments = (comments) => {
        const newDetailData = this.state.detailData
        newDetailData.comments = comments
        this.setState({ detailData: newDetailData });
    }

    onTapDetail = (post) => {
        let data = {...post, comments: []}
        this.setState({ detailData: data });
        this.getComments(post.id)
        this.setState({displayDetail: true})
    }

    deleteArticle = (id) => {
        const updatedList = this.state.result.filter((item) => item.id !== id)
        this.setState({result: updatedList, displayDetail: false})
    }

    handleChange = event => {
        this.setState({
          [event.target.id]: event.target.value
        });
    };

    handleSubmit = event => {
        this.setState({isButtonLoading: true})
        event.preventDefault();
        const newList = this.state.result
        const newPost= {
          "id": newList.length,
          "title": this.state.title,
          "body": this.state.body,
        }
        setTimeout(
            function() { 
                newList.unshift(newPost)
                this.setState({
                result: newList,
                title: "",
                body: "",
                isButtonLoading: false
                })
            }.bind(this),
            3000
        )
      };

      isSearched = searchTerm => item => {
        return item.title.toLowerCase().includes(searchTerm.toLowerCase());
      }

    render() {
        const {result, detailData, displayDetail, searchTerm} = this.state
        if (!result) { return null; }

        const columns = [{
            title: 'Title',
            dataIndex: 'title',
            key: 'title'
          },
          {
            title: 'Body',
            dataIndex: 'body',
            key: 'body'
          },
          {
            title: 'Action',
            key: 'action',
            render: data => (
              <span>
                <Button type="primary" onClick={() => this.onTapDetail(data)}>Detail</Button>
                <br/>
                <br/>
                <Button type="danger" onClick={() => this.deleteArticle(data.id)}>Delete</Button>
              </span>
            ),
          }
        ]
        const detailColums = [
            {
                title: "Name",
                dataIndex: "name",
                key: "name"
            },
            {
                title: "Email",
                dataIndex: "email",
                key: "email"
            },
            {
                title: "Body",
                dataIndex: "body",
                key: "body"
            }
        ]

        const tabChamps = [
            {label: "Title : ", id: "title", value: this.state.title, type: "text" },
            {label: "Body : ", id: "body", value: this.state.body, type: "text" }
        ];

        const dataSource = result.filter(this.isSearched(searchTerm)).slice(0,10)

        const tabChampsRecherche = [
            {label: "Recherche : ", id: "searchTerm", value: this.state.searchTerm, type: "text"}
        ]
        return (
            <div>
                <h3>Ajout :</h3>
                <Formulaire
                    changement = {this.handleChange}
                    soumission = {this.handleSubmit}
                    tabChamps = {tabChamps}
                    submitText = "Ajouter"
                    isButtonLoading = {this.state.isButtonLoading}
                ></Formulaire>
                <br/>
                <h3>Recherche : </h3>
                <Formulaire
                    changement = {this.handleChange}
                    tabChamps = {tabChampsRecherche}
                    soumission = {this.handleChange}
                    submitText = "Rechercher"
                ></Formulaire>
                <br/>
                <Table columns={columns} dataSource={dataSource} />
                {displayDetail &&
                    <Card
                    title="Detail" >
                        <p><b>Title :</b> {detailData.title}</p>
                        <p><b>Body :</b> {detailData.body}</p>
                        <p><b>Comments : </b></p>
                        <Table columns={detailColums} dataSource={detailData.comments}/>
                    </Card>
                }
            </div>
        )
    }
}
export default Articles