import React, {Component} from 'react';
import DEFAULT_STATE from '../initState';
import axios from 'axios';
import { Table, Button, Card } from 'antd';

class Albums extends Component {
    constructor(props) {
      super(props);
      this.state = DEFAULT_STATE
    }
    componentDidMount() {
        this.getAlbums()
    }

    setResult = (result) => {
        this.setState({ result: result });
    }

    getAlbums = () => {
        axios(`https://jsonplaceholder.typicode.com/albums`)
             .then(result => this.setResult(result.data))
             .catch(error => this.setState({ error }));
    }

    getImages = (album_id) => {
        axios(`https://jsonplaceholder.typicode.com/photos/?albumId=${album_id}`)
        .then(result => this.setImageAlbum(result.data))
        .catch(error => this.setState({error}));
    }

    setImageAlbum = (images) => {
        const all_image_album = this.state.imagesAlbum
        all_image_album.images = images
        this.setState({ imagesAlbum: all_image_album });
    }

    viewListImages = (data) =>{
        this.setState({ imagesAlbum: data });
        this.getImages(data.id)
        this.setState({displayImageList: true})
    }

    render() {

        const { result, imagesAlbum, displayImageList } = this.state;

        console.log(result);

        if (!result) { return null; }

        const columns = [{
            title: 'ID',
            dataIndex: 'id',
            key: 'id'
          },
          {
            title: 'Title',
            key: 'title',
            render: data => (
              <a onClick={() => this.viewListImages(data)}>{data.title}</a>
            )
          }
        ]

        const detailListColums = [
            {
                title: "ID",
                dataIndex: "id",
                key: "id"
            },
            {
                title: "Url",
                dataIndex: "url",
                key: "url"
            }
        ]
        return (
            <div>
                <Table columns={columns} dataSource={result} />
                {displayImageList &&
                    <Card
                    title="Liste des images" >
                        <p><b>Title :</b> {result.title}</p>
                        <p><b>URL :</b> {displayImageList.url}</p>
                        <Table columns={detailListColums} dataSource={imagesAlbum.images}/>
                    </Card>
                }
            </div>
        )
    }
}
export default Albums