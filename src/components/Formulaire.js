import React from 'react';
import { Form, Input, Button } from 'antd';
import {ButtonWithLoading} from './WithLoading';

const Formulaire = ({changement, soumission, tabChamps, children, submitText, isButtonLoading}) => {
    return (
        <div>
            {children}
            <Form layout="inline" onSubmit={soumission}>
            {
                tabChamps.map(element => {
                    return (
                        <Form.Item key={element.id}>
                            <Input type={element.type} id={element.id} value={element.value} onChange={changement} placeholder={element.label}/>
                        </Form.Item>
                    )
                })
            }
            <ButtonWithLoading
                isLoading={isButtonLoading}
                type="primary"
                htmlType="submit"
            >
                {submitText}
            </ButtonWithLoading>
            </Form>
        </div>
    )
}
export default Formulaire