import React, { Component } from 'react';
import logo from '../logo.svg';
import { Link } from "react-router-dom";

class Navigation extends Component {
    render() {
        return (
            <header>
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                </div>
                <nav>
                    <ul>
                        <li><Link to='/'>Home</Link></li>
                        <li><Link to='/articles'>Articles</Link></li>
                        <li><Link to='/albums'>Albums</Link></li>
                        <li><Link to='/utilisateurs'>Utilisateurs</Link></li>
                    </ul>
                </nav>
            </header>
        )
    }
}

export default Navigation