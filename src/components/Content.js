import React from 'react';
import {
  Route,
  Switch
} from 'react-router-dom';
import Accueil from '../containers/Accueil';
import Articles from '../containers/Articles';
import Albums from '../containers/Albums';
import Utilisateurs from '../containers/Utilisateurs';

const Content = () => {
    return (
        <main>
            <Switch>
                <Route exact path="/" component={Accueil}/>
                <Route exact path="/articles" component={Articles}/>
                <Route exact path="/albums" component={Albums}/>
                <Route exact path="/utilisateurs" component={Utilisateurs}/>
            </Switch>
        </main>
    )
}

export default Content