import React from 'react';
import { Button } from 'antd';

const Loading = () =>
    <Button type="primary" >Loading ...</Button>

const WithLoading = (Component) => ({ isLoading, ...props }) => {
    return isLoading
    ? <Loading />
    : <Component { ...props } />
}

export const ButtonWithLoading = WithLoading(Button);